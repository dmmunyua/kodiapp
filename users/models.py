from django.db import models
from django.contrib.auth.models import AbstractUser

from django.utils.translation import ugettext_lazy as _

from .managers import CustomUserManager


class User(AbstractUser):
	username = models.CharField(default="NotUsed", unique=False, max_length=32)
	email = models.EmailField(blank=False, max_length=254, verbose_name="email address", unique=True)
	is_manager = models.BooleanField(default=False)
	is_owner=models.BooleanField(default=True)

	USERNAME_FIELD = "email"   # e.g: "username", "email"


	REQUIRED_FIELDS = []


	objects = CustomUserManager()

	def __str__(self):
		return self.email