from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User


class CustomUserAdmin(UserAdmin):
	# override the default sort column
	ordering = ('-date_joined', )
	# if you want the date they joined or other columns displayed in the list,
	# override list_display too
	list_display = ('id','email', 'date_joined', 'first_name', 'last_name', 'is_staff', 'is_superuser', 'is_owner', 'is_manager')

	fieldsets = UserAdmin.fieldsets + (
		(None, {'fields': ('is_owner', 'is_manager',)}),
	)

# finally replace the default UserAdmin with yours
#admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)