from django.contrib import admin

# Register your models here.
from .models import Vehicle, Partner, Driver, Insurance, Insurer, Tracker, NtsaInspection, UberInspection, \
	DriverLicense, LicenseRenewal, PsvRenewal, Contract, Account, Invoice, MobilePayment, MaintenanceProvider, \
	VehicleMaintenance, SparePart, MaintenancePart, AccessToken, MpesaAccessToken, Transaction, Validation, Mileage, \
	VehicleMake, VehicleModel, CreditNote


class VehicleAdmin(admin.ModelAdmin):
	list_display = (
		'registration',
		'partner',
		'make',
		'model',
		'yom',
		'logbook',
		'chasis_no',
		'engine_no',
		#'ntsa_status',
		#'uber_status',
		'create_date',
		'update_date',
		)

class PartnerAdmin(admin.ModelAdmin):
	list_display = (
		'first_name',
		'middle_name',
		'last_name',
		'date_of_birth',
		'document_type',
		'document_number',
		'msisdn',
		'email',
		'document',
		'create_date',
		'update_date',
		'user'
	)


class DriverAdmin(admin.ModelAdmin):
	list_display = (
		'first_name',
		'middle_name',
		'last_name',
		'date_of_birth',
		'document_type',
		'document_number',
		'msisdn',
		'email',
		'status',
		'document',
		'partner',
		'create_date',
		'update_date',
	)


class InsurerAdmin(admin.ModelAdmin):
	list_display = (
		'name',
		'claim_form',
		'windscreen_form',
		'create_date',
		'update_date',
	)


class InsuranceAdmin(admin.ModelAdmin):
	list_display = (
		'insurer',
		'vehicle',
		'policy_number',
		'issue_date',
		'expiry_date',
		'status',
		'document',
		'create_date',
		'update_date',
	)


class TrackerAdmin(admin.ModelAdmin):
	list_display = (
		'id',
		'vehicle',
		'msisdn',
		'imei',
		'platform',
		'car_id',
		'expiry_date',
		'status',
		'command',
		'command_response',
		'document',
		'accesstoken',
		'create_date',
		'update_date',
	)


class NtsaInspectionAdmin(admin.ModelAdmin):
	list_display = (
		'vehicle',
		'vc_number',
		'booking_date',
		'expiry_date',
		'document',
		'create_date',
		'update_date',
	)


class UberInspectionAdmin(admin.ModelAdmin):
	list_display = (
		'vehicle',
		'booking_date',
		'expiry_date',
		'document',
		'create_date',
		'update_date',
	)


class DriverLicenseAdmin(admin.ModelAdmin):
	list_display = (
		'driver',
		'license_number',
		'license_status',
		'psv_status',
		'create_date',
		'update_date',
	)


class LicenseRenewalAdmin(admin.ModelAdmin):
	list_display = (
		'driverlicense',
		'expiry_date',
		'document',
		'create_date',
		'update_date',
	)


class PsvRenewalAdmin(admin.ModelAdmin):
	list_display = (
		'driverlicense',
		'expiry_date',
		'document',
		'create_date',
		'update_date',
	)


class ContractAdmin(admin.ModelAdmin):
	list_display = (
		'vehicle',
		'driver',
		'start_date',
		'end_date',
		'document',
		'status',
		'create_date',
		'update_date',
	)


class AccountAdmin(admin.ModelAdmin):
	list_display = (
		'contract',
		'weekly_amount',
		'weekly_run',
		'weeks',
		'mileage_based',
		'price_per_km',
		'paid_amount',
		'status',
		'create_date',
		'update_date',
	)


class InvoiceAdmin(admin.ModelAdmin):
	list_display = (
		'id',
		'account',
		'amount',
		'balance',
		'overpayment',
		'issue_date',
		'status',
		'related_invoice',
		'settled_credit_note',
		'create_date',
		'update_date',
	)


class MobilePaymentAdmin(admin.ModelAdmin):
	list_display = (
		'invoice',
		'receipt',
		'transaction_date',
		'status',
		'split',
		'index',
		'amount',
		'split_amount',
		'create_date',
		'update_date',
	)


class MaintenanceProviderAdmin(admin.ModelAdmin):
	list_display = (
		'name',
		'address',
		'msisdn',
		'status',
		'create_date',
		'update_date',
	)


class VehicleMaintenanceAdmin(admin.ModelAdmin):
	list_display = (
		'contract',
		'provider',
		'mileage',
		'amount',
		'maintenace_type',
		'maintenance_date',
		'create_date',
		'update_date',
	)


class SparePartAdmin(admin.ModelAdmin):
	list_display = (
		'name',
		'create_date',
		'update_date',
	)


class MaintenancePartAdmin(admin.ModelAdmin):
	list_display = (
		'vehiclemaintenance',
		'sparepart',
		'quantity',
		'unit_price',
		'amount',
		'create_date',
		'update_date',
	)


class AccessTokenAdmin(admin.ModelAdmin):
	list_display = (
		'username',
		'password',
		'platform',
		'access_token',
	)


class MpesaAccessTokenAdmin(admin.ModelAdmin):
	list_display = (
		'access_token',
	)


class TransactionAdmin(admin.ModelAdmin):
	list_display = (
		'TransactionType',
		'TransID',
		'FirstName',
		'MiddleName',
		'LastName',
		'BillRefNumber',
		'TransTime',
		'Reconciled',
		'ReconciledCounter',
		'Invoice',
		'Unallocated',
		'CreateDate',
		'UpdateDate',
	)
	ordering = ['-CreateDate']


class ValidationAdmin(admin.ModelAdmin):
	list_display = (
		'TransactionType',
		'TransID',
		'FirstName',
		'MiddleName',
		'LastName',
		'BillRefNumber',
		'TransTime',
		'Reconciled',
		'ReconciledCounter',
		'CreateDate',
		'UpdateDate',
	)


class MileageAdmin(admin.ModelAdmin):
	list_display = (
		'account',
		'mileage',
		'mileage_date',
		'invoiced_status',
		'create_date',
		'update_date'
	)

class VehicleMakeAdmin(admin.ModelAdmin):
	list_display = (
		'name',
		)
class VehicleModelAdmin(admin.ModelAdmin):
	list_display = (
		'name',
		'make',
		)

class CreditNoteAdmin(admin.ModelAdmin):
	list_display = (
		'id',
		'account',
		'amount',
		'related_invoice',
		'issue_date',
		'status',
		'create_date',
		'update_date',
		)



admin.site.register(Vehicle, VehicleAdmin)
admin.site.register(Partner, PartnerAdmin)
admin.site.register(Driver, DriverAdmin)
admin.site.register(Insurer, InsurerAdmin)
admin.site.register(Insurance, InsuranceAdmin)
admin.site.register(Tracker, TrackerAdmin)
admin.site.register(NtsaInspection, NtsaInspectionAdmin)
admin.site.register(UberInspection, UberInspectionAdmin)
admin.site.register(DriverLicense, DriverLicenseAdmin)
admin.site.register(LicenseRenewal, LicenseRenewalAdmin)
admin.site.register(PsvRenewal, PsvRenewalAdmin)
admin.site.register(Contract, ContractAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(MobilePayment, MobilePaymentAdmin)
admin.site.register(MaintenanceProvider, MaintenanceProviderAdmin)
admin.site.register(VehicleMaintenance, VehicleMaintenanceAdmin)
admin.site.register(SparePart, SparePartAdmin)
admin.site.register(MaintenancePart, MaintenancePartAdmin)
admin.site.register(AccessToken, AccessTokenAdmin)
admin.site.register(MpesaAccessToken, MpesaAccessTokenAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Validation, ValidationAdmin)
admin.site.register(Mileage, MileageAdmin)
admin.site.register(VehicleMake, VehicleMakeAdmin)
admin.site.register(VehicleModel, VehicleModelAdmin)
admin.site.register(CreditNote, CreditNoteAdmin)
