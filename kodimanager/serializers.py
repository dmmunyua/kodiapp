from rest_framework import serializers
from .models import Transaction, Validation


class TransactionSerializer(serializers.ModelSerializer):
    Invoice = serializers.IntegerField(required=False)
    class Meta:
        model = Transaction
        fields = ('id','TransactionType', 'TransID','Invoice','TransAmount', 'FirstName', 'MiddleName', 'LastName', 'BillRefNumber', 'TransTime')



    def create(self, validated_data):
        """
        Create and return a new `Order` instance, given the validated data.
        """
        return Transaction.objects.create(**validated_data)



class ValidationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Validation
        fields = ('id','TransactionType', 'TransID', 'TransAmount', 'FirstName', 'MiddleName', 'LastName', 'BillRefNumber', 'TransTime')



    def create(self, validated_data):
        """
        Create and return a new `Order` instance, given the validated data.
        """
        return Validation.objects.create(**validated_data)

