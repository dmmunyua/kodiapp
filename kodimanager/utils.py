import json
import requests
import datetime
from requests.exceptions import HTTPError
from decouple import config


# get_whatsgps_user_token function to fetch WhatsGPS user token
# to be used for API calls
def get_whatsgps_user_token():
    token = None
    url = config('WHATS_GPS_TOKEN_URL')
    try:
        response = requests.get(url=url,
                                params={'name': config('WHATS_GPS_USER'), 'password': config('WHATS_GPS_PASSWORD')},)

        response_dict = json.loads(response.text)
        if response.status_code != 200:
            print('Fetching user token responded with status: {}'.format(response.status_code))
            return token
        if response_dict["ret"] != 1:
            print('Fetching user token failed, ret code is: {} and the message is: {}'.format(response_dict["ret"],
                                                                                              response_dict["msg"]))
            return token
        token = response_dict["data"]["token"]
        return token
    except HTTPError as http_err:
        print('HTTP error occurred while fetching user token: {}'.format(http_err))
        return token
    except Exception as err:
        print('Some error occurred while fetching user token: {}'.format(err))
        return token


# get_total_mileage function mileage covered by a vehicle
# within a specific time
def get_total_mileage(token, car_id):
    """
    start_time and end_time should be of the previous day so as to run the job
    on the current day at 00:15 to aggregate the mileage of the whole previous day.
    """
    date_today = datetime.date.today()
    print(date_today)
    yesterday = date_today - datetime.timedelta(days=1)
    start_time = datetime.datetime.combine(yesterday, datetime.time.min)
    end_time = datetime.datetime.combine(yesterday, datetime.time(23, 59, 59))
    url = config('WHATS_GPS_MILEAGE_URL')
    mileage_fetched = False
    mileage_in_km = 0
    mileage_list = []
    try:
        response = requests.get(url=url,
                                params={'token': token, 'carId': car_id, 'startTime': start_time, 'endTime': end_time},)

        response_dict = json.loads(response.text)
        if response.status_code != 200:
            print('Fetching mileage responded with status: {}'.format(response.status_code))
            return mileage_fetched, mileage_in_km
        if response_dict["ret"] != 1:
            print('Fetching mileage failed, ret code is: {} and the message is: {}'.format(response_dict["ret"],
                                                                                           response_dict["msg"]))
            return mileage_fetched, mileage_in_km

        mileage_fetched = True
        for item in response_dict["data"]:
            mileage_list.append(item["signalMile"])

        mileage_in_km = (sum(mileage_list) / 1000)
        return mileage_fetched, mileage_in_km
    except HTTPError as http_err:
        print('HTTP error occurred while fetching mileage: {}'.format(http_err))
        return mileage_fetched, mileage_in_km
    except Exception as err:
        print('Some error occurred while fetching mileage: {}'.format(err))
        return mileage_fetched, mileage_in_km
