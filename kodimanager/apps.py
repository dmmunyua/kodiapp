from django.apps import AppConfig


class KodimanagerConfig(AppConfig):
    name = 'kodimanager'
