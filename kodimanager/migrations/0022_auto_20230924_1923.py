# Generated by Django 3.0.4 on 2023-09-24 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kodimanager', '0021_transaction_invoice'),
    ]

    operations = [
        migrations.AddField(
            model_name='invoice',
            name='related_invoice',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='status',
            field=models.CharField(choices=[('paid', 'Paid'), ('unpaid', 'Unpaid'), ('partial', 'Partial'), ('incomplete', 'Incomplete'), ('default', 'Default')], default='unpaid', max_length=16),
        ),
    ]
