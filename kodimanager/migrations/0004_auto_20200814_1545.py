# Generated by Django 3.0.4 on 2020-08-14 12:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kodimanager', '0003_auto_20200814_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='document',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='partner',
            name='document_number',
            field=models.CharField(max_length=8, null=True),
        ),
        migrations.AlterField(
            model_name='partner',
            name='middle_name',
            field=models.CharField(max_length=16, null=True),
        ),
    ]
