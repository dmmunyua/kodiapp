# Generated by Django 3.0.4 on 2020-08-16 10:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kodimanager', '0007_auto_20200816_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partner',
            name='date_of_birth',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='partner',
            name='document_type',
            field=models.CharField(blank=True, choices=[('national_id', 'National ID'), ('passport', 'Passport'), ('military_id', 'Miliary ID')], default='national_id', max_length=16, null=True),
        ),
    ]
