# Generated by Django 3.0.4 on 2020-09-02 19:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kodimanager', '0014_invoice_mileage'),
    ]

    operations = [
        migrations.AddField(
            model_name='accesstoken',
            name='platform',
            field=models.CharField(choices=[('protrack', 'ProTrackGPS'), ('whatsgps', 'WhatsGPS')], default='protrack', max_length=16),
        ),
    ]
