
from django.urls import path, re_path

from . import views



urlpatterns = [
    re_path(r'^$',views.index,name='index'),
    path('transaction/', views.Transaction.as_view()),
    path('validation/', views.Validation.as_view()),
    ]
