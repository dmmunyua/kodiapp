import graphene
import graphql_jwt
from graphene_django.types import DjangoObjectType, ObjectType
from .models import Vehicle, Partner, Driver, Insurance, Insurer, Tracker, NtsaInspection, UberInspection, DriverLicense, LicenseRenewal, \
	PsvRenewal, Contract, Account, Invoice, MobilePayment, MaintenanceProvider, VehicleMaintenance, SparePart, MaintenancePart, AccessToken, \
	MpesaAccessToken, Mileage, VehicleMake, VehicleModel

from users.models import User
import users.schema

from graphql_jwt.decorators import login_required


class CustomUserType(DjangoObjectType):
	class Meta:
		model = User

# Create a GraphQL type for the partner model
class PartnerType(DjangoObjectType):
	class Meta:
		model = Partner

# Create a GraphQL type for the vehicle model
class VehicleType(DjangoObjectType):
	class Meta:
		model = Vehicle

class DriverType(DjangoObjectType):
	class Meta:
		model = Driver

class InsurerType(DjangoObjectType):
	class Meta:
		model = Insurer

class InsuranceType(DjangoObjectType):
	class Meta:
		model = Insurance

class TrackerType(DjangoObjectType):
	class Meta:
		model = Tracker

class NtsaInspectionType(DjangoObjectType):
	class Meta:
		model = NtsaInspection

class UberInspectionType(DjangoObjectType):
	class Meta:
		model = UberInspection

class DriverLicenseType(DjangoObjectType):
	class Meta:
		model = DriverLicense

class LicenseRenewalType(DjangoObjectType):
	class Meta:
		model = LicenseRenewal

class PsvRenewalType(DjangoObjectType):
	class Meta:
		model = PsvRenewal

class ContractType(DjangoObjectType):
	class Meta:
		model = Contract

class AccountType(DjangoObjectType):
	class Meta:
		model =Account

class InvoiceType(DjangoObjectType):
	class Meta:
		model = Invoice

class MobilePaymentType(DjangoObjectType):
	class Meta:
		model = MobilePayment

class MileageType(DjangoObjectType):
	class Meta:
		model = Mileage

class VehicleMakeType(DjangoObjectType):
	class Meta:
		model = VehicleMake

class VehicleModelType(DjangoObjectType):
	class Meta:
		model = VehicleModel

#class MaintenanceProviderType(DjangoObjectType):
#	class Meta:
#		model = MaintenanceProvider

#class VehicleMaintenanceType(DjangoObjectType):
#	class Meta:
#		model = VehicleMaintenance

#class SparePartType(DjangoObjectType):
#	class Meta:
#		model = SparePart

#class MaintenancePartType(DjangoObjectType):
#	class Meta:
#		model = MaintenancePart


#Creating the Query User Class

##The first arguement is because I am importing the Users Schema. Ideally only the second arguement resides here alone
class Query(users.schema.Query, graphene.ObjectType):

	customuser = graphene.Field(CustomUserType, id=graphene.Int())
	vehicle = graphene.Field(VehicleType, id=graphene.Int(), registration=graphene.String())
	partner = graphene.Field(PartnerType, id=graphene.Int())
	driver = graphene.Field(DriverType, id=graphene.Int())
	insurer = graphene.Field(InsurerType, id=graphene.Int(), name=graphene.String())
	insurance = graphene.Field(InsuranceType, id=graphene.Int())
	tracker = graphene.Field(TrackerType, id=graphene.Int())
	ntsa_inspection = graphene.Field(NtsaInspectionType, id=graphene.Int(), vc_number=graphene.String())
	uber_inspection = graphene.Field(UberInspectionType, id=graphene.Int())
	driver_license = graphene.Field(DriverLicenseType, id=graphene.Int())
	license_renewal = graphene.Field(LicenseRenewalType, id=graphene.Int())
	psv_renewal = graphene.Field(PsvRenewalType,id=graphene.Int())
	contract = graphene.Field(ContractType, id=graphene.Int())
	account = graphene.Field(AccountType, id=graphene.Int())
	invoice = graphene.Field(InvoiceType, id=graphene.Int())
	mobile_payment = graphene.Field(MobilePaymentType, id=graphene.Int(), receipt=graphene.String())
	mileage = graphene.Field(MileageType, id=graphene.String())
	vehicle_make = graphene.Field(VehicleMakeType, id=graphene.Int())
	vehicle_model = graphene.Field(VehicleModelType, id=graphene.Int())

	#maintenance_provider = graphene.Field(MaintenanceProviderType, id=graphene.Int(), name=graphene.String())
	#vehicle_maintenance = graphene.Field(VehicleMaintenanceType, id=graphene.Int())
	#spare_part = graphene.Field(SparePartType, id=graphene.Int())
	#maintenance_part = graphene.Field(MaintenancePartType, id=graphene.Int())

	customusers = graphene.List(CustomUserType)
	vehicles = graphene.List(VehicleType)
	partners= graphene.List(PartnerType)
	drivers = graphene.List(DriverType)
	insurers = graphene.List(InsurerType)
	insurances = graphene.List(InsuranceType)
	trackers = graphene.List(TrackerType)
	ntsa_inspections = graphene.List(NtsaInspectionType)
	uber_inspections = graphene.List(UberInspectionType)
	driver_licenses = graphene.List(DriverLicenseType)
	license_renewals = graphene.List(LicenseRenewalType)
	psv_renewals = graphene.List(PsvRenewalType)
	contracts = graphene.List(ContractType)
	accounts = graphene.List(AccountType)
	invoices = graphene.List(InvoiceType)
	mobile_payments = graphene.List(MobilePaymentType)
	mileages = graphene.List(MileageType)
	vehicle_makes = graphene.List(VehicleMakeType)
	vehicle_models = graphene.List(VehicleModelType)


	#maintenance_providers = graphene.List(MaintenanceProviderType)
	#vehicle_maintenances = graphene.List(VehicleMaintenanceType)
	#spare_parts = graphene.List(SparePartType)
	#maintenance_parts = graphene.List(MaintenancePartType)

	@login_required
	def resolve_customuser(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return User.objects.get(pk=id)


	@login_required
	def resolve_partner(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return Partner.objects.get(pk=id)

		return None

	@login_required
	def resolve_vehicle(self, info, **kwargs):
		id = kwargs.get('id')
		registration = kwargs.get('registration')

		if id is not None:
			return Vehicle.objects.get(pk=id)
		
		if registration is not None:
			return Vehicle.objects.get(registration=registration)		

		return None

	@login_required
	def resolve_driver(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return Driver.objects.get(pk=id)

		return None

	@login_required	
	def resolve_insurer(self, info, **kwargs):
		id = kwargs.get('id')
		name = kwargs.get('name')

		if id is not None:
			return Insurer.objects.get(pk=id)
		if name is not None:
			return Insurer.objects.get(name=name)

		return None

	@login_required
	def resolve_insurance(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return Insurance.objects.get(pk=id)

		return None

	@login_required
	def resolve_tracker(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return Tracker.objects.get(pk=id)

		return None

	@login_required
	def resolve_ntsa_inspection(self, info, **kwargs):
		id = kwargs.get('id')
		vc_number = kwargs.get('vc_number')

		if id is not None:
			return NtsaInspection.objects.get(pk=id)
		if vc_number is not None:
			return NtsaInspection.objects.get(vc_number=vc_number)

		return None

	@login_required
	def resolve_uber_inspection(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return UberInspection.objects.get(pk=id)

		return None

	@login_required
	def resolve_driver_license(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return DriverLicense.objects.get(pk=id)

		return None

	@login_required
	def resolve_license_renewal(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return LicenseRenewal.objects.get(pk=id)

		return None

	@login_required
	def resolve_psv_renewal(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return PsvRenewal.objects.get(pk=id)

		return None

	@login_required
	def resolve_contract(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return Contract.objects.get(pk=id)

		return None

	@login_required
	def resolve_account(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return Account.objects.get(pk=id)

		return None

	@login_required
	def resolve_invoice(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return Invoice.objects.get(pk=id)

		return None

	@login_required
	def resolve_mobile_payment(self, info, **kwargs):
		id = kwargs.get('id')
		receipt = kwargs.get('receipt')

		if id is not None:
			return MobilePayment.objects.get(pk=id)
		if receipt is not None:
			return MobilePayment.objects.get(receipt=receipt)

		return None	

	@login_required
	def resolve_mileage(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return Mileage.objects.get(pk=id)

	@login_required
	def resolve_vehicle_make(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return VehicleMake.objects.get(pk=id)

	@login_required
	def resolve_vehicle_model(self, info, **kwargs):
		id = kwargs.get('id')

		if id is not None:
			return VehicleModel.objecs.get(pk=id)	

	@login_required
	def resolve_customusers(self, info, **kwargs):
		return User.objects.all()

	@login_required
	def resolve_partners(self, info, **kwargs):
		user = info.context.user
		if user.is_superuser:
			return Partner.objects.all()
		if user.is_owner:
			return Partner.objects.filter(user=user)

	@login_required
	def resolve_vehicles(self, info, **kwargs):
		user = info.context.user
		if user.is_superuser:
			return Vehicle.objects.all()
		if user.is_owner:
			return Vehicle.objects.filter(partner__user=user)

	@login_required
	def resolve_drivers(self, info, **kwargs):
		user = info.context.user
		if user.is_superuser:
			return Driver.objects.all()
		if user.is_owner:
			return Driver.objects.filter(partner__user=user)

	@login_required
	def resolve_insurers(self, info, **kwargs):
		return Insurer.objects.all()

	@login_required
	def resolve_insurances(self, info, **kwargs):
		return Insurance.objects.all()

	@login_required
	def resolve_trackers(self, info, **kwargs):
		user = info.context.user
		if user.is_superuser:
			return Tracker.objects.all()
		if user.is_owner:
			return Tracker.objects.filter(vehicle__partner__user=user)

	@login_required
	def resolve_ntsa_inspections(self, info, **kwargs):
		return NtsaInspection.objects.all()

	@login_required
	def resolve_uber_inspections(self, info, **kwargs):
		return UberInspection.objects.all()

	@login_required
	def resolve_driver_licenses(self, info, **kwargs):
		return DriverLicense.objects.all()

	@login_required
	def resolve_license_renewals(self, info, **kwargs):
		return LicenseRenewal.objects.all()

	@login_required
	def resolve_psv_renewals(self, info, **kwargs):
		return PsvRenewal.objects.all()

	@login_required
	def resolve_contracts(self, info, **kwargs):
		user = info.context.user
		if user.is_superuser:
			return Contract.objects.all()
		if user.is_owner:
			return Contract.objects.filter(vehicle__partner__user=user)

	@login_required
	def resolve_accounts(self, info, **kwargs):
		user = info.context.user
		if user.is_superuser:
			return Account.objects.all()
		if user.is_owner:
			return Account.objects.filter(contract__vehicle__partner__user=user)

	@login_required
	def resolve_invoices(self, info, **kwargs):
		user = info.context.user
		if user.is_superuser:
			return Invoice.objects.all()
		if user.is_owner:
			return Invoice.objects.filter(account__contract__vehicle__partner__user=user)

	@login_required
	def resolve_mobile_payments(self, info, **kwargs):
		return MobilePayment.objects.all()	

	@login_required
	def resolve_mileages(self, info, **kwargs):
		user = info.context.user
		if user.is_superuser:
			return Mileage.objects.all()
		if user.is_owner:
			return Mileage.objects.filter(account__contract__vehicle__partner__user=user)


	@login_required
	def resolve_vehicle_makes(self, info, **kwargs):
		return VehicleMake.objects.all()

	@login_required
	def resolve_vehicle_models(self, info, **kwargs):
		return VehicleModel.objects.all()

class CustomUserInput(graphene.InputObjectType):
	id = graphene.ID()
	email = graphene.String()
	is_owner = graphene.String()

# Create Input Object Types
class PartnerInput(graphene.InputObjectType):
	id = graphene.ID()
	first_name = graphene.String()
	middle_name = graphene.String()
	last_name = graphene.String()
	date_of_birth = graphene.DateTime()
	document_type = graphene.String()
	document_number = graphene.String()
	msisdn = graphene.Int()
	email = graphene.String()
	document = graphene.String()
	user = graphene.List(CustomUserInput)


class VehicleInput(graphene.InputObjectType):
	id = graphene.ID()
	partner = graphene.List(PartnerInput)
	registration = graphene.String()
	make = graphene.String()
	model = graphene.String()
	yom = graphene.Int()
	logbook = graphene.String()
	chasis_no = graphene.String()
	engine_no = graphene.String()
	#ntsa_status = graphene.String()
	#uber_status = graphene.String()

class DriverInput(graphene.InputObjectType):
	id = graphene.ID()
	partner = graphene.List(PartnerInput)
	first_name = graphene.String()
	middle_name = graphene.String()
	last_name = graphene.String()
	date_of_birth = graphene.DateTime()
	document_type = graphene.String()
	document_number = graphene.String()
	msisdn = graphene.Int()
	email = graphene.String()
	document = graphene.String()

class InsurerInput(graphene.InputObjectType):
	id = graphene.ID()
	name = graphene.String()
	claim_form = graphene.String()
	windscreen_form = graphene.String()


class InsuranceInput(graphene.InputObjectType):
	id = graphene.ID()
	insurer = graphene.List(InsurerInput)
	vehicle = graphene.List(VehicleInput)
	policy_number = graphene.String()
	issue_date = graphene.DateTime()
	expiry_date = graphene.DateTime()
	status = graphene.String()
	document = graphene.String()


class TrackerInput(graphene.InputObjectType):
	id = graphene.ID()
	vehicle = graphene.List(VehicleInput)
	msisdn = graphene.Int()
	imei = graphene.Int()
	platform = graphene.String()
	car_id = graphene.Int()
	expiry_date = graphene.DateTime()
	status = graphene.String()           
	command = graphene.String()   
	command_response = graphene.Boolean()   
	document = graphene.String()


class NtsaInspectionInput(graphene.InputObjectType):
	id = graphene.ID()
	vehicle = graphene.List(VehicleInput)
	vc_number = graphene.String()
	booking_date = graphene.DateTime()
	expiry_date = graphene.DateTime()
	document = graphene.String()


class UberInspectionInput(graphene.InputObjectType):
	id = graphene.ID()
	vehicle = graphene.List(VehicleInput)
	booking_date = graphene.DateTime()
	expiry_date = graphene.DateTime()
	document = graphene.String()
	

class DriverLicenseInput(graphene.InputObjectType):
	id = graphene.ID()
	driver = graphene.List(DriverInput)
	license_number = graphene.String()
	license_status = graphene.String()
	psv_status = graphene.String()


class LicenseRenewalInput(graphene.InputObjectType):
	id = graphene.ID()
	driverlicense = graphene.List(DriverLicenseInput)
	expiry_date = graphene.DateTime()
	document = graphene.String()


class PsvRenewalInput(graphene.InputObjectType):
	id = graphene.ID()
	driverlicense = graphene.List(DriverLicenseInput)
	expiry_date = graphene.DateTime()
	document = graphene.String()


class ContractInput(graphene.InputObjectType):
	id = graphene.ID()
	vehicle = graphene.List(VehicleInput)
	driver = graphene.List(DriverInput)
	start_date = graphene.DateTime()
	end_date = graphene.DateTime()
	document = graphene.String()
	status = graphene.String()


class AccountInput(graphene.InputObjectType):
	id = graphene.ID()
	contract = graphene.List(ContractInput)
	weekly_amount = graphene.Int()
	weeks = graphene.Int()
	weekly_run = graphene.String()
	mileage_based = graphene.Boolean()
	price_per_km = graphene.Int()
	paid_amount = graphene.Int()
	status = graphene.String()


class InvoiceInput(graphene.InputObjectType):
	id = graphene.ID()
	account = graphene.List(AccountInput)
	amount = graphene.Int()
	paid_amount = graphene.Int()
	balance = graphene.Int()
	related_invoice = graphene.Int()
	issue_date = graphene.DateTime()
	status = graphene.String()


class MobilePaymentInput(graphene.InputObjectType):
	id = graphene.ID()
	invoice = graphene.List(InvoiceInput)
	receipt = graphene.String()
	transaction_date = graphene.DateTime()
	status = graphene.String()
	split = graphene.Boolean()
	index = graphene.Int()
	amount = graphene.Int()
	split_amount = graphene.Int()

class VehicleMakeInput(graphene.InputObjectType):
	id = graphene.ID()
	name = graphene.String()

class VehicleModelInput(graphene.InputObjectType):
	id = graphene.ID()
	name = graphene.String()
	make = graphene.List(VehicleMakeInput)

# Create mutations for partners
class CreatePartner(graphene.Mutation):
	class Arguments:

		input = PartnerInput(required=True)

	ok = graphene.Boolean()
	partner = graphene.Field(PartnerType)

	@staticmethod
	def mutate(root, info, input=None):
		user = info.context.user
		ok = True
		partner_instance = Partner(
			first_name=input.first_name,
			middle_name = input.middle_name,
			last_name = input.last_name,
			date_of_birth = input.date_of_birth,
			document_type = input.document_type,
			document_number = input.document_number,
			msisdn = input.msisdn,
			email = input.email,
			document=input.document,
			user = user
			)
		partner_instance.save()
		return CreatePartner(ok=ok, partner=partner_instance)

class UpdatePartner(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = PartnerInput(required=True)

	ok = graphene.Boolean()
	partner = graphene.Field(PartnerType)

	@staticmethod
	def mutate(root, info, id, input=None):
		ok = False
		partner_instance = Partner.objects.get(pk=id)
		if partner_instance:
			ok = True
			if input.first_name is not None:
				partner_instance.first_name = input.first_name
			if input.middle_name is not None:
				partner_instance.middle_name = input.middle_name
			if input.last_name is not None:
				partner_instance.last_name = input.last_name
			if input.date_of_birth is not None:
				partner_instance.date_of_birth = input.date_of_birth
			if input.document_type is not None:
				partner_instance.document_type = input.document_type
			if input.document_number is not None:
				partner_instance.document_number = input.document_number
			if input.msisdn is not None:
				partner_instance.msisdn = input.msisdn
			if input.email is not None:
				partner_instance.email = input.email
			if input.document is not None:
				partner_instance.document=input.document
			partner_instance.save()
			return UpdatePartner(ok=ok, partner=partner_instance)
		return UpdatePartner(ok=ok, partner=None)


# Create mutations for vehicles
class CreateVehicle(graphene.Mutation):
	class Arguments:

		input = VehicleInput(required=True)

	ok = graphene.Boolean()
	vehicle = graphene.Field(VehicleType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		partner = []
		for partner_input in input.partner:
			is_partner = Partner.objects.get(id=partner_input.id)
			if is_partner is None:
				return CreateVehicle(ok=False, vehicle=None)
			partner.append(is_partner)

		vehicle_instance = Vehicle(
			partner = partner[0],
			registration=input.registration,
			make=input.make,
			model = input.model,
			yom = input.yom,
			logbook = input.logbook,
			chasis_no = input.chasis_no,
			engine_no = input.engine_no
			#ntsa_status = input.ntsa_status,
			#uber_status = input.uber_status
			)
		vehicle_instance.save()
		#vehicle_instance.partner.set(partner)
		return CreateVehicle(ok=ok, vehicle=vehicle_instance)

class UpdateVehicle(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = VehicleInput(required=True)

	ok = graphene.Boolean()
	vehicle = graphene.Field(VehicleType)

	@staticmethod
	def mutate(root, info, id, input=None):
		ok = False
		vehicle_instance = Vehicle.objects.get(pk=id)
		if vehicle_instance:
			ok = True
			#partner = []
			#for partner_input in input.partner:
			#	is_partner = Partner.objects.get(pk=partner_input.id)
			#	if is_partner is None:
			#		return UpdateVehicle(ok=False, vehicle=None)
			#	partner.append(is_partner)
			if input.registration is not None:
				vehicle_instance.registration=input.registration
			if input.make is not None:
				vehicle_instance.make=input.make
			if input.model is not None:
				vehicle_instance.model = input.model
			if input.yom is not None:
				vehicle_instance.yom = input.yom
			if input.logbook is not None:
				vehicle_instance.logbook = input.logbook
			if input.chasis_no is not None:
				vehicle_instance.chasis_no = input.chasis_no
			if input.engine_no is not None:
				vehicle_instance.engine_no = input.engine_no
			#if input.ntsa_status is not None:
			#	vehicle_instance.ntsa_status = input.ntsa_status
			#if input.uber_status is not None:
			#	vehicle_instance.uber_status = input.uber_status
			vehicle_instance.save()
			return UpdateVehicle(ok=ok, vehicle=vehicle_instance)
		return UpdateVehilce(ok=ok, vehicle=None)



class CreateDriver(graphene.Mutation):
	class Arguments:

		input = DriverInput(required=True)

	ok = graphene.Boolean()
	driver = graphene.Field(DriverType)

	@staticmethod
	def mutate(root, info, input=None):
		ok = True
		partner = []
		for partner_input in input.partner:
			is_partner = Partner.objects.get(id=partner_input.id)
			if is_partner is None:
				return CreateDriver(ok=False, driver=None)
			partner.append(is_partner)
		driver_instance = Driver(
			partner = partner[0],
			first_name=input.first_name,
			middle_name = input.middle_name,
			last_name = input.last_name,
			date_of_birth = input.date_of_birth,
			document_type = input.document_type,
			document_number = input.document_number,
			msisdn = input.msisdn,
			email = input.email,
			document=input.document
			)
		driver_instance.save()
		return CreateDriver(ok=ok, driver=driver_instance)

class UpdateDriver(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = DriverInput(required=True)

	ok = graphene.Boolean()
	driver = graphene.Field(DriverType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		driver_instance = Driver.objects.get(pk=id)
		if driver_instance:
			ok = True
			if input.first_name is not None:
				driver_instance.first_name = input.first_name
			if input.middle_name is not None:
				driver_instance.middle_name = input.middle_name
			if input.last_name is not None:
				driver_instance.last_name = input.last_name
			if input.date_of_birth is not None:
				driver_instance.date_of_birth = input.date_of_birth
			if input.document_type is not None:
				driver_instance.document_type = input.document_type
			if input.document_number is not None:
				driver_instance.document_number = input.document_number
			if input.msisdn is not None:
				driver_instance.msisdn = input.msisdn
			if input.email is not None:
				driver_instance.email = input.email
			if input.document is not None:
				driver_instance.document=input.document
			driver_instance.save()
			return UpdateDriver(ok=ok, driver=driver_instance)
		return UpdateDriver(ok=ok, driver=None)

#####INSURER
class CreateInsurer(graphene.Mutation):
	class Arguments:

		input = InsurerInput(required=True)

	ok = graphene.Boolean()
	insurer = graphene.Field(InsurerType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		insurer_instance = Insurer(
			name=input.name,
			claim_form = input.claim_form,
			windscreen_form = input.windscreen_form
			)
		insurer_instance.save()
		return CreateInsurer(ok=ok, insurer=insurer_instance)

class UpdateInsurer(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = InsurerInput(required=True)

	ok = graphene.Boolean()
	insurer = graphene.Field(InsurerType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		insurer_instance = Insurer.objects.get(pk=id)
		if insurer_instance:
			ok = True
			if input.name is not None:
				insurer_instance.name = input.name
			if input.claim_form is not None:
				insurer_instance.claim_form = input.claim_form
			if input.windscreen_form is not None:
				insurer_instance.windscreen_form = input.windscreen_form
			insurer_instance.save()
			return UpdateInsurer(ok=ok, insurer=insurer_instance)
		return UpdateInsurer(ok=ok, insurer=None)

####INSURANCE
class CreateInsurance(graphene.Mutation):
	class Arguments:

		input = InsuranceInput(required=True)

	ok = graphene.Boolean()
	insurance = graphene.Field(InsuranceType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		insurer = []
		vehicle = []
		for insurer_input in input.insurer:
			is_insurer = Insurer.objects.get(id=insurer_input.id)
			if is_insurer is None:
				return CreateInsurance(ok=False, insurance=None)
			insurer.append(is_insurer)	
		for vehicle_input in input.vehicle:
			is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
			if is_vehicle is None:
				return CreateInsurance(ok=False, insurance=None)
			vehicle.append(is_vehicle)			
		insurance_instance = Insurance(
			insurer = insurer[0],
			vehicle = vehicle[0],
			policy_number = input.policy_number,
			issue_date = input.issue_date,
			expiry_date = input.expiry_date,
			status = input.status,
			document = input.document
			)
		insurance_instance.save()
		return CreateInsurance(ok=ok, insurance=insurance_instance)

class UpdateInsurance(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = InsuranceInput(required=True)

	ok = graphene.Boolean()
	insurance = graphene.Field(InsuranceType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		insurance_instance = Insurance.objects.get(pk=id)
		if insurance_instance:
			ok = True
			if input.vehicle is not None:
				vehicle = []
				for vehicle_input in input.vehicle:
					is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
					if is_vehicle is None:
						return UpdateInsurance(ok=True, insurance=None)
					vehicle.append(is_vehicle)
					insurance_instance.vehicle = vehicle[0]
			if input.insurer is not None:
				insurer = []
				for insurer_input in input.insurer:
					is_insurer = Insurer.objects.get(id=insurer_input.id)
					if is_insurer is None:
						return UpdateInsurance(ok=True, insurance=None)
					insurer.append(is_insurer)
					insurance_instance.insurer = insurer[0]				
			if input.policy_number is not None:
				insurance_instance.policy_number = input.policy_number
			if input.issue_date is not None:
				insurance_instance.issue_date = input.issue_date
			if input.expiry_date is not None:
				insurance_instance.expiry_date = input.expiry_date
			if input.status is not None:
				insurance_instance.status = input.status
			if input.document is not None:
				insurance_instance.document = input.document
			insurance_instance.save()
			return UpdateInsurance(ok=ok, insurance=insurance_instance)
		return UpdateInsurance(ok=ok, insurance=None)


######TRACKER

class CreateTracker(graphene.Mutation):
	class Arguments:

		input = TrackerInput(required=True)

	ok = graphene.Boolean()
	tracker = graphene.Field(TrackerType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		vehicle = []
		for vehicle_input in input.vehicle:
			is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
			if is_vehicle is None:
				return CreateTracker(ok=False, tracker=None)
			vehicle.append(is_vehicle)		
		tracker_instance = Tracker(
			vehicle = vehicle[0],
			msisdn = input.msisdn,
			imei = input.imei,
			car_id = input.car_id,
			platform = input.platform,
			expiry_date = input.expiry_date,
			status = input.status,
			command = input.command,
			command_response = input.command_response,
			document = input.document
			)
		tracker_instance.save()
		return CreateTracker(ok=ok, tracker=tracker_instance)

class UpdateTracker(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = TrackerInput(required=True)

	ok = graphene.Boolean()
	tracker = graphene.Field(TrackerType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		tracker_instance = Tracker.objects.get(pk=id)
		if tracker_instance:
			ok = True
			if input.vehicle is not None:
				vehicle = []
				for vehicle_input in input.vehicle:
					is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
					if is_vehicle is None:
						return UpdateTracker(ok=True, tracker=None)
					vehicle.append(is_vehicle)
					tracker_instance.vehicle = vehicle[0]
			if input.msisdn is not None:
				tracker_instance.msisdn = input.msisdn
			if input.imei is not None:
				tracker_instance.imei = input.imei
			if input.command is not None:
				tracker_instance.command = input.command
			if input.command_response is not None:
				tracker_instance.command_response = input.command_response
			if input.expiry_date is not None:
				tracker_instance.expiry_date = input.expiry_date
			if input.status is not None:
				tracker_instance.status = input.status
			if input.document is not None:
				tracker_instance.document = input.document
			tracker_instance.save()
			return UpdateTracker(ok=ok, tracker=tracker_instance)
		return UpdateTracker(ok=ok, tracker=None)

class CreateNtsaInspection(graphene.Mutation):
	class Arguments:

		input = NtsaInspectionInput(required=True)

	ok = graphene.Boolean()
	ntsa_inspection = graphene.Field(NtsaInspectionType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		vehicle = []
		for vehicle_input in input.vehicle:
			is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
			if is_vehicle is None:
				return CreateNtsaInspection(ok=False, ntsa_inspection=None)
			vehicle.append(is_vehicle)		
		ntsa_inspection_instance = NtsaInspection(
			vehicle = vehicle[0],
			vc_number = input.vc_number,
			booking_date = input.booking_date,
			expiry_date = input.expiry_date,
			document = input.document
			)
		ntsa_inspection_instance.save()
		return CreateNtsaInspection(ok=ok, ntsa_inspection=ntsa_inspection_instance)


class UpdateNtsaInspection(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = NtsaInspectionInput(required=True)

	ok = graphene.Boolean()
	ntsa_inspection = graphene.Field(NtsaInspectionType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		ntsa_inspection_instance = NtsaInspection.objects.get(pk=id)
		if ntsa_inspection_instance:
			ok = True
			if input.vehicle is not None:
				vehicle = []
				for vehicle_input in input.vehicle:
					is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
					if is_vehicle is None:
						return UpdateNtsaInspection(ok=True, ntsa_inspection=None)
					vehicle.append(is_vehicle)
					ntsa_inspection_instance.vehicle = vehicle[0]
			if input.vc_number is not None:
				ntsa_inspection_instance.vc_number = input.vc_number
			if input.booking_date is not None:
				ntsa_inspection_instance.booking_date = input.booking_date
			if input.expiry_date is not None:
				ntsa_inspection_instance.expiry_date = input.expiry_date
			if input.document is not None:
				ntsa_inspection_instance.document = input.document
			ntsa_inspection_instance.save()
			return UpdateNtsaInspection(ok=ok, ntsa_inspection=ntsa_inspection_instance)
		return UpdateNtsaInspection(ok=ok, ntsa_inspection=None)

class CreateUberInspection(graphene.Mutation):
	class Arguments:

		input = UberInspectionInput(required=True)

	ok = graphene.Boolean()
	uber_inspection = graphene.Field(UberInspectionType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		vehicle = []
		for vehicle_input in input.vehicle:
			is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
			if is_vehicle is None:
				return CreateUberInspection(ok=False, uber_inspection=None)
			vehicle.append(is_vehicle)		
		uber_inspection_instance = UberInspection(
			vehicle = vehicle[0],
			booking_date = input.booking_date,
			expiry_date = input.expiry_date,
			document = input.document
			)
		uber_inspection_instance.save()
		return CreateUberInspection(ok=ok, uber_inspection=uber_inspection_instance)


class UpdateUberInspection(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = UberInspectionInput(required=True)

	ok = graphene.Boolean()
	uber_inspection = graphene.Field(UberInspectionType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		uber_inspection_instance = UberInspection.objects.get(pk=id)
		if uber_inspection_instance:
			ok = True
			if input.vehicle is not None:
				vehicle = []
				for vehicle_input in input.vehicle:
					is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
					if is_vehicle is None:
						return UpdateUberInspection(ok=True, uber_inspection=None)
					vehicle.append(is_vehicle)
					uber_inspection_instance.vehicle = vehicle[0]
			if input.booking_date is not None:
				uber_inspection_instance.booking_date = input.booking_date
			if input.expiry_date is not None:
				uber_inspection_instance.expiry_date = input.expiry_date
			if input.document is not None:
				uber_inspection_instance.document = input.document
			uber_inspection_instance.save()
			return UpdateUberInspection(ok=ok, uber_inspection=uber_inspection_instance)
		return UpdateUberInspection(ok=ok, uber_inspection=None)


class CreateDriverLicense(graphene.Mutation):
	class Arguments:

		input = DriverLicenseInput(required=True)

	ok = graphene.Boolean()
	driver_license = graphene.Field(DriverLicenseType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		driver = []
		for driver_input in input.driver:
			is_driver = Driver.objects.get(id=driver_input.id)
			if is_driver is None:
				return CreateDriverLicense(ok=False, driver_license=None)
			driver.append(is_driver)		
		driver_license_instance = DriverLicense(
			driver = driver[0],
			license_number = input.license_number,
			license_status = input.license_status,
			psv_status = input.psv_status
			)
		driver_license_instance.save()
		return CreateDriverLicense(ok=ok, driver_license=driver_license_instance)


class UpdateDriverLicense(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = DriverLicenseInput(required=True)

	ok = graphene.Boolean()
	driver_license = graphene.Field(DriverLicenseType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		driver_license_instance = DriverLicense.objects.get(pk=id)
		if driver_license_instance:
			ok = True
			if input.driver is not None:
				driver = []
				for driver_input in input.driver:
					is_driver = Driver.objects.get(id=vehicle_input.id)
					if is_driver is None:
						return UpdateDriverLicense(ok=True, driver_license=None)
					driver.append(is_driver)
					driver_license_instance.driver = driver[0]
			if input.license_number is not None:
				driver_license_instance.license_number = input.license_number
			if input.license_status is not None:
				driver_license_instance.license_status = input.license_status
			if input.psv_status is not None:
				driver_license_instance.psv_status = input.psv_status
			driver_license_instance.save()
			return UpdateDriverLicense(ok=ok, driver_license=driver_license_instance)
		return UpdateDriverLicense(ok=ok, driver_license=None)

class CreateLicenseRenewal(graphene.Mutation):
	class Arguments:

		input = LicenseRenewalInput(required=True)

	ok = graphene.Boolean()
	license_renewal = graphene.Field(LicenseRenewalType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		driverlicense = []
		for driverlicense_input in input.driverlicense:
			is_driverlicense = DriverLicense.objects.get(id=driverlicense_input.id)
			if is_driverlicense is None:
				return CreateLicenseRenewal(ok=False, license_renewal=None)
			driverlicense.append(is_driverlicense)		
		license_renewal_instance = LicenseRenewal(
			driverlicense = driverlicense[0],
			expiry_date = input.expiry_date,
			document = input.document
			)
		license_renewal_instance.save()
		return CreateLicenseRenewal(ok=ok, license_renewal=license_renewal_instance)


class UpdateLicenseRenewal(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = LicenseRenewalInput(required=True)

	ok = graphene.Boolean()
	license_renewal = graphene.Field(LicenseRenewalType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		license_renewal_instance = LicenseRenewal.objects.get(pk=id)
		if license_renewal_instance:
			ok = True
			if input.driverlicense is not None:
				driverlicense = []
				for driverlicense_input in input.driver_license:
					is_driverlicense = DriverLicense.objects.get(id=driverlicense_input.id)
					if is_driverlicense is None:
						return UpdateLicenseRenewal(ok=True, license_renewal=None)
					driverlicense.append(is_driverlicense)
					license_renewal_instance.driver = driverlicense[0]
			if input.expiry_date is not None:
				license_renewal_instance.expiry_date = input.expiry_date
			if input.document is not None:
				license_renewal_instance.document = input.document
			license_renewal_instance.save()
			return UpdateLicenseRenewal(ok=ok, license_renewal=license_renewal_instance)
		return UpdateLicenseRenewal(ok=ok, license_renewal=None)


class CreatePsvRenewal(graphene.Mutation):
	class Arguments:

		input = PsvRenewalInput(required=True)

	ok = graphene.Boolean()
	psv_renewal = graphene.Field(PsvRenewalType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		driverlicense = []
		for driverlicense_input in input.driverlicense:
			is_driverlicense = DriverLicense.objects.get(id=driverlicense_input.id)
			if is_driverlicense is None:
				return CreatePsvRenewal(ok=False, psv_renewal=None)
			driverlicense.append(is_driverlicense)		
		psv_renewal_instance = PsvRenewal(
			driverlicense = driverlicense[0],
			expiry_date = input.expiry_date,
			document = input.document
			)
		psv_renewal_instance.save()
		return CreatePsvRenewal(ok=ok, psv_renewal=psv_renewal_instance)


class UpdatePsvRenewal(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = PsvRenewalInput(required=True)

	ok = graphene.Boolean()
	psv_renewal = graphene.Field(PsvRenewalType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		psv_renewal_instance = PsvRenewal.objects.get(pk=id)
		if psv_renewal_instance:
			ok = True
			if input.driverlicense is not None:
				driverlicense = []
				for driverlicense_input in input.driver_license:
					is_driverlicense = DriverLicense.objects.get(id=driverlicense_input.id)
					if is_driverlicense is None:
						return UpdatePsvRenewal(ok=True, psv_renewal=None)
					driverlicense.append(is_driverlicense)
					psv_renewal_instance.driver = driverlicense[0]
			if input.expiry_date is not None:
				psv_renewal_instance.expiry_date = input.expiry_date
			if input.document is not None:
				psv_renewal_instance.document = input.document
			psv_renewal_instance.save()
			return UpdatePsvRenewal(ok=ok, psv_renewal=psv_renewal_instance)
		return UpdatePsvRenewal(ok=ok, psv_renewal=None)

class CreateContract(graphene.Mutation):
	class Arguments:

		input = ContractInput(required=True)

	ok = graphene.Boolean()
	contract = graphene.Field(ContractType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		driver = []
		vehicle = []
		for driver_input in input.driver:
			is_driver = Driver.objects.get(id=driver_input.id)
			if is_driver is None:
				return CreateContract(ok=False, contract=None)
			driver.append(is_driver)	
		for vehicle_input in input.vehicle:
			is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
			if is_vehicle is None:
				return CreateContract(ok=False, contract=None)
			vehicle.append(is_vehicle)			
		contract_instance = Contract(
			driver = driver[0],
			vehicle = vehicle[0],
			start_date = input.start_date,
			end_date = input.end_date,
			document = input.document,
			status = input.status
			)
		contract_instance.save()
		return CreateContract(ok=ok, contract=contract_instance)

class UpdateContract(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = ContractInput(required=True)

	ok = graphene.Boolean()
	contract = graphene.Field(ContractType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		contract_instance = Contract.objects.get(pk=id)
		if contract_instance:
			ok = True
			if input.vehicle is not None:
				vehicle = []
				for vehicle_input in input.vehicle:
					is_vehicle = Vehicle.objects.get(id=vehicle_input.id)
					if is_vehicle is None:
						return UpdateInsurance(ok=True, insurance=None)
					vehicle.append(is_vehicle)
					contract_instance.vehicle = vehicle[0]
			if input.driver is not None:
				driver = []
				for driver_input in input.driver:
					is_driver = Driver.objects.get(id=driver_input.id)
					if is_driver is None:
						return UpdateContract(ok=True, contract=None)
					driver.append(is_driver)
					contract_instance.driver = driver[0]				
			if input.start_date is not None:
				contract_instance.start_date = input.start_date
			if input.end_date is not None:
				contract_instance.end_date = input.end_date
			if input.document is not None:
				contract_instance.document = input.document
			if input.status is not None:
				contract_instance.status = input.status
			contract_instance.save()
			return UpdateContract(ok=ok, contract=contract_instance)
		return UpdateContract(ok=ok, contract=None)


class CreateAccount(graphene.Mutation):
	class Arguments:

		input = AccountInput(required=True)

	ok = graphene.Boolean()
	account = graphene.Field(AccountType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		contract = []
		for contract_input in input.contract:
			is_contract = Contract.objects.get(id=contract_input.id)
			if is_contract is None:
				return CreateAccount(ok=False, account=None)
			contract.append(is_contract)			
		account_instance = Account(
			contract = contract[0],
			weekly_amount = input.weekly_amount,
			weeks = input.weeks,
			paid_amount = input.paid_amount,
			status = input.status
			)
		account_instance.save()
		return CreateAccount(ok=ok, account=account_instance)

class UpdateAccount(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = AccountInput(required=True)

	ok = graphene.Boolean()
	account = graphene.Field(AccountType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		account_instance = Account.objects.get(pk=id)
		if account_instance:
			ok = True
			if input.contract is not None:
				contract = []
				for contract_input in input.contract:
					is_contract = Contract.objects.get(id=contract_input.id)
					if is_contract is None:
						return UpdateAccount(ok=True, account=None)
					contract.append(is_contract)
					account_instance.contract = contract[0]				
			if input.weekly_amount is not None:
				account_instance.weekly_amount = input.weekly_amount
			if input.weeks is not None:
				account_instance.weeks = input.weeks
			if input.paid_amount is not None:
				account_instance.paid_amount = input.paid_amount
			if input.status is not None:
				account_instance.status = input.status
			account_instance.save()
			return UpdateAccount(ok=ok, account=account_instance)
		return UpdateAccount(ok=ok, account=None)


class CreateInvoice(graphene.Mutation):
	class Arguments:

		input = InvoiceInput(required=True)

	ok = graphene.Boolean()
	invoice = graphene.Field(InvoiceType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		account = []
		for account_input in input.account:
			is_account = Account.objects.get(id=account_input.id)
			if is_account is None:
				return CreateInvoice(ok=False, invoice=None)
			account.append(is_account)			
		invoice_instance = Invoice(
			account = account[0],
			amount = input.amount,
			paid_amount = input.paid_amount,
			balance = input.balance,
			related_invoice = input.related_invoice,
			issue_date = input.issue_date,
			status = input.status
			)
		invoice_instance.save()
		return CreateInvoice(ok=ok, invoice=invoice_instance)

class UpdateInvoice(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = InvoiceInput(required=True)

	ok = graphene.Boolean()
	invoice = graphene.Field(InvoiceType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		invoice_instance = Invoice.objects.get(pk=id)
		if invoice_instance:
			ok = True
			if input.account is not None:
				account = []
				for account_input in input.account:
					is_account = Account.objects.get(id=account_input.id)
					if is_account is None:
						return UpdateInvoice(ok=True, invoice=None)
					account.append(is_account)
					invoice_instance.account = account[0]				
			if input.amount is not None:
				invoice_instance.amount = input.amount
			if input.paid_amount is not None:
				invoice_instance.paid_amount = input.paid_amount
			if input.balance is not None:
				invoice_instance.balance = input.balance
			if input.related_invoice is not None:
				invoice_instance.related_invoice = input.related_invoice
			if input.issue_date is not None:
				invoice_instance.issue_date = input.issue_date
			if input.status is not None:
				invoice_instance.status = input.status
			invoice_instance.save()
			return UpdateInvoice(ok=ok, invoice=invoice_instance)
		return UpdateInvoice(ok=ok, invoice=None)


class CreateMobilePayment(graphene.Mutation):
	class Arguments:

		input = MobilePaymentInput(required=True)

	ok = graphene.Boolean()
	mobile_payment = graphene.Field(MobilePaymentType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		invoice = []
		for invoice_input in input.invoice:
			is_invoice = Invoice.objects.get(id=invoice_input.id)
			if is_invoice is None:
				return CreateMobilePayment(ok=False, mobile_payment=None)
			invoice.append(is_invoice)			
		mobile_payment_instance = MobilePayment(
			invoice = invoice[0],
			receipt = input.receipt,
			transaction_date = input.transaction_date,
			amount = input.amount,
			split = input.split,
			index = input.index,
			split_amount = input.split_amount,
			status = input.status
			)
		mobile_payment_instance.save()
		return CreateMobilePayment(ok=ok, mobile_payment=mobile_payment_instance)

class UpdateMobilePayment(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = MobilePaymentInput(required=True)

	ok = graphene.Boolean()
	mobile_payment = graphene.Field(MobilePaymentType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		mobile_payment_instance = MobilePayment.objects.get(pk=id)
		if mobile_payment_instance:
			ok = True
			if input.invoice is not None:
				invoice = []
				for invoice_input in input.invoice:
					is_invoice = Invoice.objects.get(id=invoice_input.id)
					if is_invoice is None:
						return UpdateMobilePayment(ok=True, mobile_payment=None)
					invoice.append(is_invoice)
					mobile_payment_instance.invoice = invoice[0]				
			if input.receipt is not None:
				mobile_payment_instance.receipt = input.receipt
			if input.amount is not None:
				mobile_payment_instance.amount = input.amount				
			if input.transaction_date is not None:
				mobile_payment_instance.transaction_date = input.transaction_date
			if input.split is not None:
				mobile_payment_instance.split = input.split
			if input.index is not None:
				mobile_payment_instance.index = input.index
			if input.split_amount is not None:
				mobile_payment_instance.split_amount = input.split_amount	
			if input.status is not None:
				mobile_payment_instance.status = input.status
			mobile_payment_instance.save()
			return UpdateMobilePayment(ok=ok, mobile_payment=mobile_payment_instance)
		return UpdateMobilePayment(ok=ok, mobile_payment=None)



class CreateVehicleMake(graphene.Mutation):
	class Arguments:

		input = VehicleMakeInput(required=True)

	ok = graphene.Boolean()
	vehicle_make = graphene.Field(VehicleMakeType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
			
		vehicle_make_instance = VehicleMake(
			name = input.name
			)
		vehicle_make_instance.save()
		return CreateVehicleMake(ok=ok, vehicle_make=vehicle_make_instance)

class UpdateVehicleMake(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = VehicleMakeInput(required=True)

	ok = graphene.Boolean()
	vehicle_make = graphene.Field(VehicleMakeType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		vehicle_make_instance = VehicleMake.objects.get(pk=id)
				
		if vehicle_make_instance:
			ok=True

			if input.name is not None:
				vehicle_make_instance.name = input.name
			vehicle_make_instance.save()
			return UpdateVehicleMake(ok=ok, vehicle_make=vehicle_make_instance)
		return UpdateVehicleMake(ok=ok, vehicle_make=None)



class CreateVehicleModel(graphene.Mutation):
	class Arguments:

		input = VehicleModelInput(required=True)

	ok = graphene.Boolean()
	vehicle_model = graphene.Field(VehicleModelType)

	@staticmethod
	@login_required
	def mutate(root, info, input=None):
		ok = True
		vehicle_make = []
		for vehicle_make_input in input.vehicle_make:
			is_vehicle_make = VehicleMake.objects.get(id=vehicle_make_input.id)
			if is_vehicle_make is None:
				return CreateVehicleModel(ok=False, vehicle_model=None)
			vehicle_make.append(is_vehicle_make)			
		vehicle_model_instance = VehicleModel(
			make = vehicle_make[0],
			name = input.name,
			)
		vehicle_model_instance.save()
		return CreateVehicleModel(ok=ok, vehicle_model=vehicle_model_instance)

class UpdateVehicleModel(graphene.Mutation):
	class Arguments:
		id = graphene.Int(required=True)
		input = VehicleModelInput(required=True)

	ok = graphene.Boolean()
	vehicle_model = graphene.Field(VehicleModelType)

	@staticmethod
	@login_required
	def mutate(root, info, id, input=None):
		ok = False
		vehicle_model_instance = VehicleModel.objects.get(pk=id)
		if vehicle_model_instance:
			ok = True
			if input.vehicle_make is not None:
				vehicle_make = []
				for vehicle_make_input in input.vehicle_make:
					is_vehicle_make = VehicleMake.objects.get(id=vehicle_make_input.id)
					if is_vehicle_make is None:
						return UpdateVehicleModel(ok=True, vehicle_model=None)
					vehicle_make.append(is_vehicle_make)
					vehicle_model_instance.make = vehicle_make[0]				
			if input.name is not None:
				vehicle_model_instance.name = input.name
			vehicle_model_instance.save()
			return UpdateVehicleModel(ok=ok, vehicle_model=vehicle_model_instance)
		return UpdateVehicleModel(ok=ok, vehicle_model=None)


# Create mutations

##The first arguement is because I am importing the Users Schema. Ideally only the second arguement resides here alone
class Mutation(users.schema.Mutation, graphene.ObjectType):
	create_vehicle = CreateVehicle.Field()
	update_vehicle = UpdateVehicle.Field()
	create_partner = CreatePartner.Field()
	update_partner = UpdatePartner.Field()
	create_driver = CreateDriver.Field()
	update_driver = UpdateDriver.Field()
	create_insurance = CreateInsurance.Field()
	update_insurance = UpdateInsurance.Field()
	create_insurer = CreateInsurer.Field()
	update_insurer = UpdateInsurer.Field()
	create_tracker = CreateTracker.Field()
	update_tracker = UpdateTracker.Field()
	create_ntsa_inspection = CreateNtsaInspection.Field()
	update_ntsa_inspection = UpdateNtsaInspection.Field()
	create_uber_inspection = CreateUberInspection.Field()
	update_uber_inspection = UpdateUberInspection.Field()
	create_driver_license = CreateDriverLicense.Field()
	update_driver_license = UpdateDriverLicense.Field()
	create_license_renewal = CreateLicenseRenewal.Field()
	update_license_renewal = UpdateLicenseRenewal.Field()
	create_psv_renewal = CreatePsvRenewal.Field()
	update_psv_renewal = UpdatePsvRenewal.Field()
	create_contract = CreateContract.Field()
	update_contract = UpdateContract.Field()
	create_account = CreateAccount.Field()
	update_account = UpdateAccount.Field()	
	create_invoice = CreateInvoice.Field()
	update_invoice = UpdateInvoice.Field()
	create_mobile_payment = CreateMobilePayment.Field()
	update_mobile_payment = UpdateMobilePayment.Field()
	create_vehicle_make = CreateVehicleMake.Field()
	update_vehicle_make = UpdateVehicleMake.Field()
	create_vehicle_model = CreateVehicleModel.Field()
	update_vehicle_model = UpdateVehicleModel.Field()

	#Variables to support JWT
	#token_auth = graphql_jwt.ObtainJSONWebToken.Field() #TokenAuth is used to authenticate the User with its username and password to obtain the JSON Web token.
	#verify_token = graphql_jwt.Verify.Field() #VerifyToken to confirm that the token is valid, passing it as an argument.
	#refresh_token = graphql_jwt.Refresh.Field() #RefreshToken to obtain a new token within the renewed expiration time for non-expired tokens, if they are enabled to expire.


schema = graphene.Schema(query=Query, mutation=Mutation)