#from django.contrib.auth.mixins import LoginRequiredMixin
#from graphene_django.views import GraphQLView


from django.shortcuts import render


from .models import Transaction as TransactionModel
from .models import Validation as ValidationModel
from rest_framework import generics
from rest_framework.views import APIView
from .serializers import TransactionSerializer, ValidationSerializer

# Create your views here.


#from django.http import JsonResponse, HttpResponse
#from django.views.decorators.csrf import csrf_exempt
#from rest_framework.parsers import JSONParser

#from kodimanager.tasks import make_payment


def index(request):
	return render(request, 'template.html')


#class PrivateGraphQLView(LoginRequiredMixin, GraphQLView):
#	pass


#@csrf_exempt
#def payment(request):
#	if request.method == 'POST':
#		data = JSONParser().parse(request)
#		make_payment.delay(data)
#		return JsonResponse({'response_code': '0', 'response_status': 'Success'})
#	else:
#		return JsonResponse({'response_code': 'E1','response_status': 'Invalid Request'})

class Transaction(generics.ListCreateAPIView):
    queryset = TransactionModel.objects.all()
    serializer_class = TransactionSerializer

class Validation(generics.ListCreateAPIView):
    queryset = ValidationModel.objects.all()
    serializer_class = ValidationSerializer