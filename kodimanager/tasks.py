from __future__ import absolute_import, unicode_literals

from celery import shared_task
from celery.utils.log import get_task_logger

from .models import Contract, Invoice, Account, Tracker, Vehicle, AccessToken, MpesaAccessToken, Driver, Transaction, Invoice, MobilePayment, Mileage, CreditNote
from django.utils import timezone
from django.db.models import Q
from .utils import get_whatsgps_user_token, get_total_mileage

from requests.auth import HTTPBasicAuth

from twilio.rest import Client

from django.conf import settings

from decouple import config

import datetime
import requests
import json

import time
import hashlib

import re

import africastalking

logger = get_task_logger(__name__)

@shared_task()
def generate_invoice():

	#Get day of week as an integer
	day_of_week = datetime.datetime.today().weekday()
	
	#If Day of week is less than 6 then add 1 to match celery task day of week
	if (day_of_week < 6):
		day_of_run = str(day_of_week + 1)
	#Otherwise Day of Week is 6 in Python, a Sunday, which is a 0 in Celery Task
	else:
		day_of_run = str(0)

	try:
		accounts = Account.objects.filter(Q(status='active') | Q(status='delayed'), Q(mileage_based=False), Q(weekly_run=day_of_run))
		for account in accounts:
			invoice = Invoice.objects.create(account=account, amount=account.weekly_amount, balance=account.weekly_amount, mileage=0, issue_date=timezone.now())
			invoice.save()
			driver = Driver.objects.get(contract__account = account.id)
			
			recipient = driver.msisdn
			message_amount = account.weekly_amount
			#Africa's talking SMS method (has prefix at)
			at_send_invoice_sms.delay(message_amount, invoice.id, recipient)
		logger.info('generate_invoice Completed')
	except:
		logger.info('generate_invoice FAILED')

@shared_task()
def check_overdue_invoice_switchoff():
	#try:
	invoices = Invoice.objects.filter(Q(status='unpaid'), issue_date__lt=timezone.now())

	for invoice in invoices:
		#Get corresponding tracker of motor vehicle
		tracker = Tracker.objects.get(vehicle__contract__account__invoice = invoice.id)

		#Calculate difference between invoice issue date and task run date
		invoice_create_date = invoice.create_date #Changed to Create Date rather than Issue Date to Accomodate What was Previously Partial
		invoice_issue_date = invoice.issue_date #Added to Cover What Was Previously Partial
		task_date = timezone.now()
		diff_create = task_date - invoice_create_date
		diff_issue = task_date - invoice_issue_date



		#Get corresponding driver of motor vehicle for purpose of sending message advice of SWITCH OFF
		driver = Driver.objects.get(contract__account__invoice = invoice.id)
		recipient = driver.msisdn

		#for invoices that remain unpaid for more than 1 day the tracker is switched off
		#print(diff.days)
		if (invoice.status=='unpaid') and (diff_create.days>=1):
			if (tracker.status=='on') and (tracker.autoswitch==True):
				tracker.command_response=False
				tracker.save()
				send_tracker_command_off.delay(tracker.id)
				#Africa's talking SMS method (has prefix at)
				at_send_tracker_off_sms.delay(recipient)
			else:
				pass
		#For invoices with partial payments run additional checks of percentage of cash received at specified intervals ## Irrelevant
		elif (invoice.status=='unpaid') and (diff_issue.days>=1):
			#percentage = ((invoice.paid_amount/invoice.amount)*100) ##Irrelevant after updates to what was Previously Partial
			#print(percentage)

			if (diff_issue.days>=2):
				if (tracker.status=='on') and (tracker.autoswitch==True):
					tracker.command_response=False
					tracker.save()
					send_tracker_command_off.delay(tracker.id)
					#Africa's talking SMS method (has prefix at)
					at_send_tracker_off_sms.delay(recipient)
				else:
					pass
			else:
				pass
		else:
			pass
	logger.info('check_overdue_invoice Completed')
	#except:
	#	logger.info('check_overdue_invoice FAILED')




@shared_task()
def send_tracker_command_off(tracker_id):
	try:

		tracker = Tracker.objects.get(id=tracker_id)
		access = AccessToken.objects.get(tracker=tracker.id)

		token = access.access_token
		imei = tracker.imei
		command = 'RELAY,1'



		url = config('PROTRACK_COMMAND_URL') #URL to be changed to http://api.protrack365.com/api/command/send
		payload = {'access_token': token, 'imei': imei, 'command': command}
		response = requests.post(url, params=payload)
		jsonResponse = response.json()
		tracker.command = jsonResponse['record']['commandid']
		tracker.save()
		logger.info('send_tracker_command_off Completed')
		get_result_tracker_command_off.apply_async(args=[tracker.id], countdown=7)
	except:
		logger.info('send_tracker_command_off FAILED')

	


@shared_task()
def get_result_tracker_command_off(tracker_id):

	try:
		tracker = Tracker.objects.get(id=tracker_id)
		access = AccessToken.objects.get(tracker=tracker.id)

		token = access.access_token
		commandid = tracker.command
		
		url = config('PROTRACK_QUERY_OFF_URL') #URL to be changed to http://api.protrack365.com/api/command/query
		payload = {'access_token': token, 'commandid': commandid}
		response = requests.post(url, params=payload)
		jsonResponse = response.json()

		query_response = jsonResponse['record']['response']
		query_off = query_response.startswith('Stop Engine Success')
		if (query_off==True):
			tracker.status='off'
			tracker.command_response = True
			tracker.save()
			logger.info('Tracker OFF Success')
		else:
			logger.info('Tracker OFF Failed')
			

		logger.info('get_result_tracker_command_off Completed')
	except:
		logger.info('get_result_tracker_command_off FAILED')



@shared_task()
def get_protrack_access_token():
	try:
		#Get Token Instance
		
		tokens = AccessToken.objects.filter(platform='protrack')

		for token in tokens:
			#Get unix time for signature
			unixtime = int(time.time())

			#Port this to a DB since multiple accounts may be involved
			tracker_account = token.username
			password = token.password

			#Calculate md5 signature hash for password and get hexadecimal value
			hashpassword = hashlib.md5(password.encode())
			hexpassword = hashpassword.hexdigest()

			#Concatenate hashed password and unix time value
			concatenatepassword = hexpassword + str(unixtime)

			#calculate md5 signature hash for concatenated string and get hexadecimal value
			rawsignature = hashlib.md5(concatenatepassword.encode())
			signature = rawsignature.hexdigest()

			#Call the Protrack GPS API
			url=config('PROTRACK_AUTH_URL')
			#url="http://api.protrack365.com/api/authorization"
			payload = {'time': unixtime, 'account': tracker_account, 'signature': signature}
			response = requests.get(url, params=payload)
			jsonResponse = response.json()

			try:
				#save the access token provided
				token.access_token = jsonResponse['record']['access_token']
				token.save()
			except:
				token.access_token = 'Error Occured'
				token.save()
				logger.info('get__protrack_access_token Token Failed')
			else:
				logger.info('get__protrack_access_token Token OK')
		logger.info('get_protrack_access_token Completed')
	except:
		logger.info('get_protrack_access_token FAILED')



@shared_task()
def get_whatsgps_access_token():
	try:
		tokens = AccessToken.objects.filter(platform='whatsgps')

		for token in tokens:
			url = config('WHATS_GPS_TOKEN_URL')
			payload = {'name': token.username, 'password': token.password}
			response = requests.get(url, params=payload)
			jsonResponse = response.json()

			try:
				#save the access token provided
				token.access_token = jsonResponse['data']['token']
				token.save()
			except:
				token.access_token = 'Error Occured'
				token.save()
				logger.info('get_whatsgps_access_token Token Failed')
			else:
				logger.info('get_whatsgps_access_token Token OK')
	except:
		logger.info('get_whatsgps_access_token FAILED')





@shared_task()
def get_mpesa_access_token():

	try:
		tokens = MpesaAccessToken.objects.get(id=1)

		if token.exists():

			consumer_key = "KeKQV2dcpHGj5dAAsOyBvsFxwr8rCDYt"
			consumer_secret = "7FJbHh5ANH8dRAGN"
			url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials"
			
			response = requests.get(url, auth=HTTPBasicAuth(consumer_key, consumer_secret))
			jsonResponse = response.json()

			token.access_token = jsonResponse['access_token']
			token.save()
			logger.info('get_mpesa_access_token Completed')
	except:
		logger.info('get_mpesa_access_token FAILED')



@shared_task()
def send_invoice_sms(message_amount, invoice_id, recipient):

	day_of_week = datetime.datetime.today().weekday()

	if (day_of_week==0):
		day_of_pay = 'Tuesday'
	elif (day_of_week==1):
		day_of_pay = 'Wednesday'
	elif (day_of_week==2):
		day_of_pay = 'Thursday'
	elif (day_of_week==3):
		day_of_pay = 'Friday'
	elif (day_of_week==4):
		day_of_pay = 'Saturday'
	elif (day_of_week==5):
		day_of_pay = 'Sunday'
	else:
		day_of_pay = 'Monday'

	try:
		account_sid = settings.TWILIO_ACCOUNT_SID
		auth_token = settings.TWILIO_AUTH_TOKEN
		client = Client(account_sid, auth_token)
		message_body = "Your invoice number " + str(invoice_id) + " of Ksh. " + str(message_amount) + " is ready. Please pay before " + day_of_pay + " 3.30am or vehicle will be switched OFF"
		message_recipient = "+254" + str(recipient)

		message = client.messages.create(
			from_='+12014821812',
			body=message_body,
			to=message_recipient
	     )

		print(message.sid)
	except:
		logger.info('send_invoice_sms FAILED')

@shared_task()
def send_tracker_off_sms(recipient):
	try:

		account_sid = settings.TWILIO_ACCOUNT_SID
		auth_token = settings.TWILIO_AUTH_TOKEN
		client = Client(account_sid, auth_token)
		message_body = "Your vehicle has been switched OFF"
		message_recipient = "+254" + str(recipient)

		message = client.messages.create(
			from_='+12014821812',
			body=message_body,
			to=message_recipient
	     )

		print(message.sid)
	except:
		logger.info('send_tracker_off_sms FAILED')

@shared_task()
def send_tracker_command_on(tracker_id):

	try:
		tracker = Tracker.objects.get(id=tracker_id)
		access = AccessToken.objects.get(tracker=tracker.id)

		token = access.access_token
		imei = tracker.imei
		command = 'RELAY,0'


		url = config('PROTRACK_COMMAND_URL') #URL to be changed to http://api.protrack365.com/api/command/send
		payload = {'access_token': token, 'imei': imei, 'command': command}
		response = requests.post(url, params=payload)
		jsonResponse = response.json()
		tracker.command = jsonResponse['record']['commandid']
		tracker.save()
		logger.info('send_tracker_command_on Completed')
		get_result_tracker_command_on.apply_async(args=[tracker.id], countdown=7)
	except:
		logger.info('send_tracker_command_on FAILED')

	


@shared_task()
def get_result_tracker_command_on(tracker_id):

	try:
		tracker = Tracker.objects.get(id=tracker_id)
		access = AccessToken.objects.get(tracker=tracker.id)

		token = access.access_token
		commandid = tracker.command
		
		url = config('PROTRACK_QUERY_ON_URL') #URL to be changed to http://api.protrack365.com/api/command/query
		payload = {'access_token': token, 'commandid': commandid}
		response = requests.post(url, params=payload)
		jsonResponse = response.json()

		query_response = jsonResponse['record']['response']
		query_on = query_response.startswith('Restore Engine Success')
		if (query_on==True):
			tracker.status = 'on'
			tracker.command_response = True
			tracker.save()
			logger.info('Tracker ON Success')
		else:
			logger.info('Tracker ON Failed')
			

		logger.info('get_result_tracker_command_on Completed')
	except:
		logger.info('get_result_tracker_command_on FAILED')

@shared_task()
def send_tracker_on_sms(recipient):

	try:
		account_sid = settings.TWILIO_ACCOUNT_SID
		auth_token = settings.TWILIO_AUTH_TOKEN
		client = Client(account_sid, auth_token)
		message_body = "Your vehicle has been switched ON"
		message_recipient = "+254" + str(recipient)

		message = client.messages.create(
			from_=config('TWILIO_SEND_MSISDN'),
			body=message_body,
			to=message_recipient
	     )

		print(message.sid)
	except:
		logger.info('send_tracker_on_sms FAILED')



@shared_task()
def check_vehicles_off_if_invoice_paid():

	#Get trackers that are in the off state
	trackers = Tracker.objects.filter(status='off')

	#Check if trackers exist in the off state
	if trackers.exists():

		#iterate through the tracker objects to get a single record
		for tracker in trackers:

			#count number of overdue invoices
			overdue_invoice_count = Invoice.objects.filter(Q(account__contract__vehicle__tracker=tracker.id), (Q(status='unpaid'))).count()
			
			#if no overdue invoices then switch on the car and send sms
			if (overdue_invoice_count == 0):
				drivers = Driver.objects.filter(Q(contract__vehicle__tracker = tracker.id), Q(status='active'))
				for driver in drivers:
					recipient = driver.msisdn
					send_tracker_command_on.delay(tracker.id)
					#Africa's talking SMS method (has prefix at)
					at_send_tracker_on_sms.delay(recipient)


@shared_task()
def reconcile_transaction():
	#Fetch all transactions that are not reconciled and the counter=1
	transactions = Transaction.objects.filter(Reconciled=False, ReconciledCounter=0)

	#Check if any record within the object exists otherwise pass
	if transactions.exists():
		for transaction in transactions:
#			try:
#				BillRefNumber = int(transaction.BillRefNumber)
#			except:
#				logger.info('Transaction has string input as bill ref number')

			BillRefNumber = transaction.BillRefNumber.replace(" ","")
			BillRefNumber = BillRefNumber.upper()

			p = re.compile(r"^k\D\D\d\d\d\D", re.IGNORECASE)
			q = p.match(BillRefNumber)

			if (q):
				vehicles = Vehicle.objects.filter(registration=BillRefNumber)
				vehicles_count = vehicles.count()

				if (vehicles_count > 0):
					for vehicle in vehicles:
						invoices = Invoice.objects.filter(Q(account__contract__vehicle=vehicle.id), (Q(status='unpaid')))

						for invoice in invoices:
							
							invoice_amount = invoice.amount
							invoice_balance = invoice.balance
							

							#If invoice amount to be paid matches paid amount
							if (invoice_amount == transaction.TransAmount):
								invoice.paid_amount = transaction.TransAmount
								invoice.balance = 0
								invoice.status = 'paid'
								invoice.save()
								transaction.Reconciled = True
								transaction.Invoice = invoice
								transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								transaction.save()

							#if invoice amount to be paid exceeds paid amount	
							elif (invoice_amount > transaction.TransAmount):

								#If invoice status is unpaid
								#if(invoice.status == 'unpaid'):
								invoice.paid_amount = transaction.TransAmount
								invoice.balance = invoice.amount - transaction.TransAmount
								invoice.status = 'partial'
								invoice.save()
								transaction.Reconciled = True
								transaction.Invoice = invoice
								transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								transaction.save()
								###Following changes to how to reconcile transactions the below code is irrelevant


								#if invoice status is partly paid
								#elif (invoice.status == 'partial'):

									#if invoice balance matches paid amount
								#	if (invoice_balance == transaction.TransAmount):
								#		invoice.paid_amount = invoice.paid_amount + transaction.TransAmount
								#		invoice.balance = 0
								#		invoice.status = 'paid'
								#		invoice.save()
								#		transaction.Reconciled = True
								#		transaction.Invoice = invoice
								#		transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								#		transaction.save()
									#if invoice balance exceeds paid amount
								#	elif (invoice_balance > transaction.TransAmount):
								#		invoice.paid_amount = invoice.paid_amount + transaction.TransAmount
								#		invoice.balance = invoice.amount - invoice.paid_amount
								#		invoice.status = 'partial'
								#		invoice.save()
								#		transaction.Reconciled = True
								#		transaction.Invoice = invoice
								#		transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								#		transaction.save()

									#if invoice balance is less than paid amount	
								#	else:
								#		invoice_allocation = invoice_balance
								#		invoice.paid_amount = invoice.paid_amount + invoice_allocation
								#		invoice.balance = 0
								#		invoice.status = 'paid'
								#		invoice.save()
								#		transaction.Reconciled = True
								#		transaction.Invoice = invoice
								#		transaction.Unallocated = transaction.TransAmount - invoice_allocation
								#		transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								#		transaction.save()
								#if invoice status is already paid
								#else:
								#	transaction.Reconciled = True
								#	transaction.Unallocated = transaction.TransAmount
								#	transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								#	transaction.save()
							#If invoice amount to be paid is less than paid amount
							else:
								invoice_allocation = invoice_amount
								invoice.paid_amount = invoice_allocation
								invoice.balance = 0
								invoice.overpayment = transaction.TransAmount - invoice_allocation
								invoice.status = 'overpaid'
								invoice.save()
								transaction.Reconciled = True
								transaction.Invoice = invoice
								transaction.Unallocated = transaction.TransAmount - invoice_allocation
								transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								transaction.save()

								#create the credit note
								creditnote_account = invoice.account
								creditnote_amount = invoice.overpayment
								creditnote_invoice = invoice.id

								creditnote = CreditNote.objects.create(account=creditnote_account, amount=creditnote_amount, related_invoice=creditnote_invoice, issue_date=timezone.now())
								creditnote.save()
				else:
					transaction.ReconciledCounter = transaction.ReconciledCounter + 1
					transaction.save()
			else:
				transaction.ReconciledCounter = transaction.ReconciledCounter + 1
				transaction.save()





@shared_task()
def unallocated_amounts_to_unpaid_invoices():
	pass


@shared_task()
def get_mileage_per_vehicle():
	#whatsgps_token = get_whatsgps_user_token()

	#if whatsgps_token is not None:
	try:
		accounts = Account.objects.filter(Q(status='active') & Q(mileage_based=True))
		for account in accounts:

			"""Fetch tracker to get access to car_id which will be used to get mileage of a vehicle."""
			vehicle = Vehicle.objects.get(contract__account=account.id)
			tracker = Tracker.objects.get(vehicle=vehicle.id)
			token = AccessToken.objects.get(tracker=tracker.id)

			# Fetch a day's mileage from WhatsGPS tracker
			if (tracker.platform.lower() != 'whatsgps'):
				logger.info('get_mileage_per_vehicle STOPPED, tracker type is not WhatsGPS')
				#mileage_fetched is a flag to check if API call to fetch mileage succeeded(True) otherwise it is False. Default value for mileage is 0 KM."""
			mileage_fetched, mileage = get_total_mileage(token.access_token, tracker.car_id)
			if mileage_fetched is True:
				# Populate the mileage table
				mileage_object = Mileage()
				mileage_object.account = account
				mileage_object.mileage = mileage
				mileage_object.mileage_date = (datetime.date.today()) - datetime.timedelta(days=1)
				# mileage_object.invoiced_status = False
				mileage_object.save()
				logger.info('get_mileage_per_vehicle Completed')
				#return 'mileage fetch succeeded'
			logger.info('get_mileage_per_vehicle STOPPED, mileage value is 0 KM')
			#return 'mileage fetch failed'
	except Exception as err:
		logger.info('get_mileage_per_vehicle FAILED with error: {}'.format(err))
		#return 'mileage fetch failed'
	#logger.info('get_mileage_per_vehicle FAILED, whatsgps_token is None')
	#return 'mileage fetch failed'


@shared_task()
def generate_mileage_based_invoice():
	# Monday is 0 and Sunday is 6
	day_of_week = datetime.datetime.today().weekday()

	if day_of_week < 6:
		day_of_run = str(day_of_week + 1)
	else:
		day_of_run = str(0)
	try:
		accounts = Account.objects.filter(Q(status='active') | Q(status='delayed'), Q(mileage_based=True), Q(weekly_run=day_of_run))

		if accounts.exists():
			for account in accounts:
				price_per_km = account.price_per_km
				minimum_amount = int(account.weekly_amount)
				mileage_values = []

				# Get mileage records for the past 7 days which are not invoiced.
				mileage_objects = Mileage.objects.filter(Q(invoiced_status=False) & Q(account=account) & Q(create_date__lt=datetime.datetime.today()) & Q(create_date__gte=datetime.datetime.today()-datetime.timedelta(days=7)))
				#print(mileage_objects)
				if mileage_objects.exists():
					# Getting the total mileages per account.
					for mileage_object in mileage_objects:
						mileage_values.append(mileage_object.mileage)

					# Calculating the total cost  and generating an invoice.
					total_mileage = int(round(sum(mileage_values)))
					total_amount = int(price_per_km) * total_mileage
					if total_amount <= minimum_amount:
						total_amount = minimum_amount
					#invoice.account = account
					#invoice.amount = total_amount
					#invoice.balance = total_amount
					#invoice.issue_date = timezone.now()
					invoice = Invoice.objects.create(account=account, amount=total_amount, balance=total_amount, mileage=total_mileage, issue_date=timezone.now())
					invoice.save()

					"""After a successful invoice generation, update the mileage objects and set invoiced_status to true."""
					mileage_objects.update(invoiced_status=True)

					# Sending an invoice to a driver.
					driver = Driver.objects.get(contract__account=account.id)
					recipient = driver.msisdn
					message_amount = total_amount
					send_mileage_invoice_sms.delay(message_amount, invoice.id, total_mileage, recipient)
					#logger.info('generate_mileage_based_invoice successful')
				logger.info('no mileage objects were found')
			logger.info('generate_mileage_based_invoice COMPLETED')
		logger.info('mileage based invoice task completed, no account objects were found')
	except Exception as err:
		logger.info('generate_mileage_based_invoice FAILED with error: {}'.format(err))


@shared_task()
def send_mileage_invoice_sms(message_amount, invoice_id, mileage, recipient):

	day_of_week = datetime.datetime.today().weekday()

	if (day_of_week==0):
		day_of_pay = 'Tuesday'
	elif (day_of_week==1):
		day_of_pay = 'Wednesday'
	elif (day_of_week==2):
		day_of_pay = 'Thursday'
	elif (day_of_week==3):
		day_of_pay = 'Friday'
	elif (day_of_week==4):
		day_of_pay = 'Saturday'
	elif (day_of_week==5):
		day_of_pay = 'Sunday'
	else:
		day_of_pay = 'Monday'

	message_body = "Your invoice number " + str(invoice_id) + " of Ksh. " + str(message_amount) + " is ready. Please pay before " + day_of_pay + " 6am. You travelled " + str(mileage) + " kms."

	try:
		account_sid = settings.TWILIO_ACCOUNT_SID
		auth_token = settings.TWILIO_AUTH_TOKEN
		client = Client(account_sid, auth_token)
		message_body = "Your invoice number " + str(invoice_id) + " of Ksh. " + str(message_amount) + " is ready. Please pay before " + day_of_pay + " 6am. You travelled " + str(mileage) + " kms."
		message_recipient = "+254" + str(recipient)

		message = client.messages.create(
			from_='+12014821812',
			body=message_body,
			to=message_recipient
	     )

		print(message.sid)
	except:
		logger.info('send_invoice_sms FAILED')

#Africa's Talking (AT) SMS Code

#Test code for AT SMS

#@shared_task
#def test_sms():

	# Initialize SDK
#	username = settings.AFRICATALKING_USERNAME
#	api_key = settings.AFRICASTALKING_APIKEY
#	africastalking.initialize(username, api_key)

#	recipients = ["+254725803169"]
#	message = "TEST123"
#	sender = "UWEZO-INFO"

	# Initialize a service e.g. SMS
#	sms = africastalking.SMS
	# Use the service synchronously
#	response = sms.send(message, recipients, sender)
#	print(response)


@shared_task()
def at_send_invoice_sms(message_amount, invoice_id, recipient):

	day_of_week = datetime.datetime.today().weekday()

	if (day_of_week==0):
		day_of_pay = 'Tuesday'
	elif (day_of_week==1):
		day_of_pay = 'Wednesday'
	elif (day_of_week==2):
		day_of_pay = 'Thursday'
	elif (day_of_week==3):
		day_of_pay = 'Friday'
	elif (day_of_week==4):
		day_of_pay = 'Saturday'
	elif (day_of_week==5):
		day_of_pay = 'Sunday'
	else:
		day_of_pay = 'Monday'

	try:
		username = settings.AFRICASTALKING_USERNAME
		api_key = settings.AFRICASTALKING_APIKEY
		sender = settings.AFRICASTALKING_SENDERID
		africastalking.initialize(username, api_key)
		message_body = "Your invoice number " + str(invoice_id) + " of Ksh. " + str(message_amount) + " is ready. Please pay before " + day_of_pay + " 3.30am or vehicle will be switched OFF"
		message_recipient = "+254" + str(recipient)

		message = message_body
		recipients = [message_recipient]

		#initialize a service e.g. SMS
		sms = africastalking.SMS

		#use the service synchronously
		response = sms.send(message, recipients, sender)

		print(response)
	except:
		logger.info('at_send_invoice_sms FAILED')

@shared_task()
def at_send_tracker_off_sms(recipient):
	try:

		username = settings.AFRICASTALKING_USERNAME
		api_key = settings.AFRICASTALKING_APIKEY
		sender = settings.AFRICASTALKING_SENDERID
		africastalking.initialize(username, api_key)
		message_body = "Your vehicle has been switched OFF"
		message_recipient = "+254" + str(recipient)

		message = message_body
		recipients = [message_recipient]

		#initialize a service e.g. SMS
		sms = africastalking.SMS

		#use the service synchronously
		response = sms.send(message, recipients, sender)

		print(response)
	except:
		logger.info('at_send_tracker_off_sms FAILED')

	


@shared_task()
def at_send_tracker_on_sms(recipient):

	try:
		username = settings.AFRICASTALKING_USERNAME
		api_key = settings.AFRICASTALKING_APIKEY
		sender = settings.AFRICASTALKING_SENDERID
		africastalking.initialize(username, api_key)
		message_body = "Your vehicle has been switched ON"
		message_recipient = "+254" + str(recipient)

		message = message_body
		recipients = [message_recipient]

		#initialize a service e.g. SMS
		sms = africastalking.SMS

		#use the service synchronously
		response = sms.send(message, recipients, sender)

		print(response)
	except:
		logger.info('at_send_tracker_on_sms FAILED')



@shared_task()
def create_new_invoice_partial_invoices():
	try:

		invoices = Invoice.objects.filter(status='partial')

		if invoices.exists():
			for invoice in invoices:
				invoice_account = invoice.account
				invoice_balance = invoice.balance
				invoice_id = invoice.id 
				invoice_issue_date = invoice.issue_date


				new_invoice = Invoice.objects.create(account=invoice_account, amount=invoice_balance, balance=invoice_balance, related_invoice=invoice_id, issue_date=invoice_issue_date, mileage=0)
				new_invoice.save()

				invoice.status = 'paid'
				invoice.save()
				logger.info ('Invoice ID ' + str(invoice.id) + ' Re-issued after partial payment')

		else:
			logger.info('No Partial Invoices')

		logger.info('create_new_invoice_partial_invoices COMPLETED')
	except Exception as e:
		print(e)
		logger.info('create_new_invoice_partial_invoices FAILED')


@shared_task()
def settle_credit_notes_from_newly_issued_invoices():

	try:

		invoices = Invoice.objects.filter(Q(status='unpaid') | Q(status='partial'))

		if invoices.exists():
			for invoice in invoices:
				pending_creditnote_count = CreditNote.objects.filter(status='pending').count()

				if (pending_creditnote_count > 0):

					creditnotes = CreditNote.objects.filter(Q(account = invoice.account), Q(status='pending'))

					for creditnote in creditnotes:
						invoice_amount = invoice.amount
						invoice_balance = invoice.balance

						if (invoice_amount == creditnote.amount):
							invoice.paid_amount = creditnote.amount
							invoice.balance = 0
							invoice.status = 'paid'
							invoice.settled_credit_note = creditnote.id 
							invoice.save()

							creditnote.status = 'settled'
							creditnote.save()

						elif (invoice_amount > creditnote.amount):
							invoice.paid_amount = creditnote.amount
							invoice.balance = invoice.amount - creditnote.amount 
							invoice.status = 'partial'
							invoice.settled_credit_note = creditnote.id 
							invoice.save()

							creditnote.status = 'settled'
							creditnote.save()

						else:
							invoice_allocation = invoice.amount 
							invoice.paid_amount = invoice_allocation
							invoice.balance = 0
							invoice.overpayment = creditnote.amount - invoice_allocation
							invoice.status = 'overpaid'
							invoice.settled_credit_note =  creditnote.id 
							invoice.save()

							creditnote.status = 'settled'
							creditnote.save()
				else:
					logger.info('No Pending Credit Notes')

		else:
			logger.info('No Unpaid or Partial Invoices')

		logger.info('settle_credit_notes_from_newly_issued_invoices COMPLETED')
	except Exception as e:
		print(e)
		logger.info('settle_credit_notes_from_newly_issued_invoices FAILED')


