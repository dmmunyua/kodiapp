def reconcile_transaction():
	#try:
	#Fetch all transactions that are not reconciled and the counter=0
	transactions = Transaction.objects.filter(Reconciled=False, ReconciledCounter=0)

	#Check if any record within the object exists otherwise pass
	if transactions.exists():

		#Loop through the objects to obtain individual records
		for transaction in transactions:

			#check if the billrefnumber is an int -MIGHT NEED CONVERT TO A STRING????
			try:
			
				BillRefNumber = int(transaction.BillRefNumber)
			except:
				logger.info('Transaction has string input as bill ref number')
				transaction.ReconciledCounter = transaction.ReconciledCounter + 1
				transaction.save()

			else:

				#Fetch an invoice whose if matches the billref number
				try:
					invoice = Invoice.objects.get(id=BillRefNumber)
				except:
					logger.info('Invoice referenced does not exist')
					transaction.ReconciledCounter = transaction.ReconciledCounter + 1
					transaction.save()
				else:
					#Check if a record exists otherwise set amount to unallocated
					#if invoice.exists():
					invoice_amount = invoice.amount
					invoice_balance = invoice.balance

					#If invoice amount to be paid matches paid amount
					if (invoice_amount == transaction.TransAmount):
						invoice.paid_amount = transaction.TransAmount
						invoice.balance = 0
						invoice.status = 'paid'
						invoice.save()
						transaction.Reconciled = True
						transaction.ReconciledCounter = transaction.ReconciledCounter + 1
						transaction.save()

					#if invoice amount to be paid exceeds paid amount	
					elif (invoice_amount > transaction.TransAmount):

						#If invoice status is unpaid
						if (invoice.status == 'unpaid'):
							invoice.paid_amount = transaction.TransAmount
							invoice.balance = invoice.amount - transaction.TransAmount
							invoice.status = 'partial'
							invoice.save()
							transaction.Reconciled = True
							transaction.ReconciledCounter = transaction.ReconciledCounter + 1
							transaction.save()
						#if invoice status is partly paid
						elif (invoice.status == 'partial'):

							#if invoice balance matches paid amount
							if (invoice_balance == transaction.TransAmount):
								invoice.paid_amount = invoice.paid_amount + transaction.TransAmount
								invoice.balance = 0
								invoice.status = 'paid'
								invoice.save()
								transaction.Reconciled = True
								transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								transaction.save()
							#if invoice balance exceeds paid amount
							elif (invoice_balance > transaction.TransAmount):
								invoice.paid_amount = invoice.paid_amount + transaction.TransAmount
								invoice.balance = invoice.amount - invoice.paid_amount
								invoice.status = 'partial'
								invoice.save()
								transaction.Reconciled = True
								transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								transaction.save()

							#if invoice balance is less than paid amount	
							else:
								invoice_allocation = invoice_balance
								invoice.paid_amount = invoice.paid_amount + invoice_allocation
								invoice.balance = 0
								invoice.status = 'paid'
								invoice.save()
								transaction.Reconciled = True
								transaction.Unallocated = transaction.TransAmount - invoice_allocation
								transaction.ReconciledCounter = transaction.ReconciledCounter + 1
								transaction.save()
						#if invoice status is already paid
						else:
							transaction.Reconciled = True
							transaction.Unallocated = transaction.TransAmount
							transaction.ReconciledCounter = transaction.ReconciledCounter + 1
							transaction.save()
					#If invoice amount to be paid is less than paid amount
					else:
						invoice_allocation = invoice_amount
						invoice.paid_amount = invoice_allocation
						invoice.balance = 0
						invoice.status = 'paid'
						invoice.save()
						transaction.Reconciled = True
						transaction.Unallocated = transaction.TransAmount - invoice_allocation
						transaction.ReconciledCounter = transaction.ReconciledCounter + 1
						transaction.save()

				
	logger.info('reconcile_transaction Completed')
	#except:
	#	logger.info('reconcile_transaction FAIL



	curl -X POST https://api.twilio.com/2010-04-01/Accounts/AC4392466256004825ecce0964dcfa6abf/Messages.json \
--data-urlencode "Body=Your vehicle has been switched ON. Pay early to avoid mishaps." \
--data-urlencode "From=+12014821812" \
--data-urlencode "To=+254720127684" \
-u AC4392466256004825ecce0964dcfa6abf:a7f0e857b087b88fe76783ff0fa53239


curl -X POST https://api.twilio.com/2010-04-01/Accounts/AC4392466256004825ecce0964dcfa6abf/Messages.json \
--data-urlencode "Body=Your vehicle will be switched off if you haven't settled the invoice of Ksh.8,000 by tomorrow morning." \
--data-urlencode "From=+12014821812" \
--data-urlencode "To=++254708788648" \
-u AC4392466256004825ecce0964dcfa6abf:a7f0e857b087b88fe76783ff0fa53239



0033_auto_20200813_2205.py

# Generated by Django 3.0.4 on 2020-08-13 19:05
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
class Migration(migrations.Migration):
    dependencies = [
    	migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('kodimanager', '0032_auto_20200812_2042'),
    ]
    operations = [
        migrations.AlterField(
            model_name='partner',
            name='user',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]


0033_auto_20200813_2205


# Generated by Django 3.0.4 on 2020-08-13 19:05
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('kodimanager', '0032_auto_20200812_2042'),
    ]
    operations = [
        migrations.AlterField(
            model_name='partner',
            name='user',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
